# DBT Demo
## Scope
* This a brief intro to the features and resultant benefits of DBT. While this by no means covers everything, it highlights some of the key reasons I fell in love with DBT.  
* Data was in the form of the Postgres [DVD Rentals DB](https://www.postgresqltutorial.com/postgresql-sample-database/#http://www.postgresqltutorial.com/postgresql-sample-database/#).  
* DB is hosted locally.

## Features
### Autogenerated DBT Docs. 
* You can find these hosted [here](https://determined-hamilton-8aaa60.netlify.app/#!/model/model.dbt_demo.fact_rentals)
* By populating the schema.yml files with the relevant information you can generate table documentation:
```yaml
    models:
    - name: fact_rentals
      description: "Incremental table of rentals on a rental ID level of granularity"
      columns:
          - name: rental_date
            description: "timestamp of rental"
          - name: rental_id
            description: "PK: Unique ID per rental"
```
* The documentation also provides you with very useful dependancy graphs between your models. These can really help when accessing the impact of potential changes to upstream models.
![lineage graph](https://i.ibb.co/yysndQ3/Screenshot-2020-07-09-at-12-34-23.png)

### Incremental Models
* Refreshing very large tables can be expensive, particularly when the SQL involves complex calculation.
* Incremental models can reduce this cost by only updating/populating rows that need to be.
* In this example I have assumed historical records will not change over time and therefore we only need to populate new rows rather than refresh the entire table.
* Assuming the model is set to refresh every 24hrs, this means we only need to update the previous days rentals.
```sql
--fact_rentals.sql
{{
    config(
        materialized='incremental',
        unique_key='rental_id'
    )
}}

...

{% if is_incremental() %}
WHERE stage_1.rental_date >= CURRENT_DATE - 1
{% endif %}
```

### Tests
* This has to be one of my favourite features of DBT, the ability to use both in-built tests as well as write your own.
* Not only does this aid with model creation but also allows you to continually verify that all models are behaving as expected while in productions (no unexpected changes to the raw data or upstream models).

**In Built Tests**
* Tests include column uniqueness, not NULL, accepted values.
```yaml
# schema.yml
- name: rental_id
            tests:
                - unique
                - not_null
- name: film_rating
    tests:
      - accepted_values:
          values: ["PG", "R", "G", "PG-13", "NC-17"]
```
**Custom Tests**
* Custom tests have to written such that they return NULL. If any value is returned the test fails.
* While this is little limiting with a bit of ingenuity you can write fairly sophisticated tests.
* A simple example for reconciling the revenue across two tables:
```sql
-- rentals_daily_rev_reconciliation.sql
WITH staging_1 AS (
SELECT
	'fact_rentals' AS source_name
	, -SUM(fact_rentals.payment_amount) AS revenue

FROM {{ref('fact_rentals')}} AS fact_rentals
GROUP BY 1
UNION ALL
SELECT
	'payments' AS source_name
	, SUM(payment.amount) AS revenue

FROM dvdrental.public.payment AS payment
GROUP BY 1
)

, staging_2 AS (
SELECT
	SUM(revenue) revenue

FROM staging_1
)

SELECT *
FROM staging_2
WHERE revenue != 0
```

### Macros
* Some macros are provided by DBT while you can also write your own.
* These can be used to autogenerate frequently used code snippets. In the past I have even used macros for entire scripts that were frequently used but needed a few variables changed each time.
* Below is a simple macro used to hash together certain columns to create a unique PK for the table. This can aid with the testing outlined above, particularly for uniqueness, as well as debugging.
```sql
-- pk_hash.sql
{% macro pk_hash(columns = []) %}
md5(ROW(
{% for col in columns %}
{{col}} {% if not loop.last %} , {% endif %}
{% endfor %}
)::TEXT)
{% endmacro %}
```
* This takes a list as an input and hashes together each element and returns a random int.
* This can them be reused in any scripts as so:
```sql
-- rentals_regional_monthly.sql
SELECT
	*
	, {{pk_hash(columns = ['rental_month', 'film_category', 'country', 'city'])}} AS _pk

FROM staging_1
```

### Simple schema management.
* Organising and managing schemas is a breeze in DBT. Within your `dbt_project.yml` file you can define schema names as well as default materialisations eg. view vs table:
```yaml
models:
  dbt_demo:
      commercial:
          materialized: table
          +schema: commercial
```
* In this case anything model within the `commercial` directory is automatically populated as a table within the commercial schema. Is this doesn't already exist it will be created.
* This easy organisation is particularly useful as your warehouse grows, helping scaling.

