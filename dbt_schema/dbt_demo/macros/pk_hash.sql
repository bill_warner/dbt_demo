{% macro pk_hash(columns = []) %}
md5(ROW(
{% for col in columns %}
{{col}} {% if not loop.last %} , {% endif %}
{% endfor %}
)::TEXT)
{% endmacro %}