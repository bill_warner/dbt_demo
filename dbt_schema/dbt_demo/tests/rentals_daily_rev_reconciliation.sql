WITH staging_1 AS (
SELECT
	'fact_rentals' AS source_name
	, -SUM(fact_rentals.payment_amount) AS revenue

FROM {{ref('fact_rentals')}} AS fact_rentals
GROUP BY 1
UNION ALL

SELECT
	'payments' AS source_name
	, SUM(payment.amount) AS revenue

FROM dvdrental.public.payment AS payment
GROUP BY 1
)

, staging_2 AS (
SELECT
	SUM(revenue) revenue

FROM staging_1
)

SELECT *

FROM staging_2
WHERE revenue != 0
