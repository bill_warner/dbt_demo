{{
    config(
        materialized='incremental',
        unique_key='rental_id'
    )
}}
WITH staging_1 AS (
SELECT
	rental.rental_date
	, rental.rental_id
	, rental.return_date
	, film.title AS film_name
	, film.description AS film_description
	, film.release_year AS film_release_year
	, film.replacement_cost AS film_replacement_cost
	, film.rating AS film_rating
	, category.name AS film_category
	, customer_snapshot.customer_id
	, customer_snapshot.first_name AS customer_first_name
	, customer_snapshot.last_name AS customer_last_name
	, customer_snapshot.email AS customer_email
	, SUM(payment.amount) AS payment_amount

FROM dvdrental.public.rental AS rental
LEFT JOIN dvdrental.public.inventory AS inventory
ON rental.inventory_id = inventory.inventory_id
LEFT JOIN dvdrental.public.film AS film
ON inventory.film_id = film.film_id
LEFT JOIN dvdrental.public.film_category AS film_category
ON film.film_id = film_category.film_id
LEFT JOIN dvdrental.public.category AS category
ON film_category.category_id = category.category_id
LEFT JOIN {{ref('customer_snapshot')}} AS customer_snapshot
ON rental.customer_id = customer_snapshot.customer_id
AND rental.rental_date BETWEEN customer_snapshot.dbt_valid_from AND customer_snapshot.dbt_valid_to
LEFT JOIN dvdrental.public.payment AS payment
ON rental.rental_id = payment.rental_id
GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13
)

SELECT
	*
	, ROW_NUMBER() OVER (PARTITION BY stage_1.customer_id ORDER BY stage_1.rental_date ASC) AS customer_rental_number

FROM staging_1 AS stage_1
{% if is_incremental() %}
WHERE stage_1.rental_date >= CURRENT_DATE - 1
{% endif %}