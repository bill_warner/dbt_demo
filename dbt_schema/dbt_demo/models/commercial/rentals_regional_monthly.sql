{{
    config(
        materialized='view',
    )
}}

WITH staging_1 AS (
SELECT
	DATE_TRUNC('MONTH', fact_rentals.rental_date) AS rental_month
	, film_category
	, country.country
	, city.city
	, SUM(fact_rentals.payment_amount) AS revenue
	, COUNT(DISTINCT fact_rentals.rental_id) rental_count
	, COUNT(DISTINCT CASE WHEN fact_rentals.customer_rental_number = 1 THEN fact_rentals.customer_id END) first_time_renter_count

FROM {{ref('fact_rentals')}} AS fact_rentals
LEFT JOIN dvdrental.public.customer AS customer
ON fact_rentals.customer_id = customer.customer_id
LEFT JOIN dvdrental.public.address AS address
ON customer.address_id = address.address_id
LEFT JOIN dvdrental.public.city AS city
ON address.city_id = city.city_id
LEFT JOIN dvdrental.public.country AS country
ON city.country_id = country.country_id

GROUP BY 1,2,3,4
)

SELECT
	*
	, {{pk_hash(columns = ['rental_month', 'film_category', 'country', 'city'])}} AS _pk

FROM staging_1