{% snapshot customer_snapshot %}

    {{
        config(
          target_schema='snapshots',
          strategy='timestamp',
          unique_key='customer_id',
          updated_at='last_update',
        )
    }}

SELECT
	customer.customer_id
	, customer.store_id
	, customer.first_name
	, customer.last_name
	, customer.email
	, customer.address_id
	, customer.activebool
	, customer.create_date
	, customer.last_update
	, customer.active

FROM dvdrental.public.customer AS customer

{% endsnapshot %}
